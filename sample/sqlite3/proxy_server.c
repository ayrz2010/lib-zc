/*
 * ================================
 * eli960@qq.com
 * https://blog.csdn.net/eli960
 * 2017-08-02
 * ================================
 */

#include "zc.h"

int main(int argc, char **argv)
{
    return zsqlite3_proxy_server_main(argc, argv);
}
